<?php


class Personal {

	public $firstName;
	public $middleName;
	public $lastName;


	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printFullName() {
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$personal = new Personal('Christian', 'Yaun', 'Ocanada');


class Developer extends Personal {

	public function printFullName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a Developer.";
	}
}

$developer = new Developer('John', 'Smith', 'Doe');


class Engineer extends Personal {

	public function printFullName() {
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
}

$engineer = new Engineer('Eduarde', 'Aguilar', 'Sumacot');