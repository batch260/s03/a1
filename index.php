<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Activity</title>
</head>
<body>

	<h2>Personal</h2>
	<p><?php echo $personal->printFullName(); ?></p>

	<h2>Developer</h2>
	<p><?php echo $developer->printFullName(); ?></p>

	<h2>Engineer</h2>
	<p><?php echo $engineer->printFullName(); ?></p>


</body>
</html>